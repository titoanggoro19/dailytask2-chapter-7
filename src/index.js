import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./Module";
import Styled from "./Styled";
import "./index.css";
import Header from "./components/Header";
import Carousel from "./components/Carousel";
import Content1 from "./components/Content1";
import Content2 from "./components/Content2";
import Content from "./components/Content";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Header />
    <Carousel />
    <Content />
    <Content1 />
    <Content2 />
  </React.StrictMode>
);
