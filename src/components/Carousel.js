import "../index";
import { Carousel } from "antd";

const contentStyle = {
  height: "711px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

const imagetStyle = {
  height: "720px",
  width: "100%",
  textAlign: "center",
};

export default () => (
  <Carousel autoplay>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://images5.alphacoders.com/122/thumb-1920-1228594.png"
          alt=""
        />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://images.alphacoders.com/879/thumb-1920-87963.jpg"
          alt=""
        />
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>
        <img
          style={imagetStyle}
          src="https://images3.alphacoders.com/614/thumb-1920-614743.png"
          alt=""
        />
      </h3>
    </div>
  </Carousel>
);
