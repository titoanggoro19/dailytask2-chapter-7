import React from "react";
import "antd/dist/antd.css";
import "./style.css";
import { Typography } from "antd";

const { Title } = Typography;

export default () => (
  <Typography className="typography">
    <Title>h1. Ant Design</Title>
    <Title level={2}>h2. Ant Design</Title>
    <Title level={3}>h3. Ant Design</Title>
    <Title level={4}>h4. Ant Design</Title>
    <Title level={5}>h5. Ant Design</Title>
  </Typography>

  
);
