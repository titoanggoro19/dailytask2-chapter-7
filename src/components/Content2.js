import React from "react";
import "antd/dist/antd.css";
import "./style.css";
import { Typography } from "antd";

const { Title } = Typography;

const contentStylecolor = {
  color: "#fff",
};

export default () => (
  <Typography className="typography-dark">
    <Title style={contentStylecolor}>h1. Ant Design</Title>
    <Title style={contentStylecolor} level={2}>
      h2. Ant Design
    </Title>
    <Title style={contentStylecolor} level={3}>
      h3. Ant Design
    </Title>
    <Title style={contentStylecolor} level={4}>
      h4. Ant Design
    </Title>
    <Title style={contentStylecolor} level={5}>
      h5. Ant Design
    </Title>
  </Typography>
);
